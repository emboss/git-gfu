function print(msg) {
  console.log(msg)
}

function main() {
  // prints hello world
  if (ENV['LANG'] === 'DE') {
    print('Hallo, Welt.')
  } else {
    print('Hello, world.')
  }
}

